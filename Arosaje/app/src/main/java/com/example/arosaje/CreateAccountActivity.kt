package com.example.arosaje

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject


class CreateAccountActivity  : BaseActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creation_compte)
        showBack()
        setHeaderTitle(getString(R.string.txtCreate))

        sharedPreferences = getSharedPreferences("user_account", Context.MODE_PRIVATE)

        val editFirstName=findViewById<EditText>(R.id.editTextFirstName)
        val editLastName=findViewById<EditText>(R.id.editTextLastName)
        val editUsername= findViewById<EditText>(R.id.editTextUsername)
        val editEmail=findViewById<EditText>(R.id.editTextEmail)
        val editPassword=findViewById<EditText>(R.id.editTextPwd)

        val buttonCreate= findViewById<Button>(R.id.buttonCreate)

        buttonCreate.setOnClickListener {

            // Sauvegarder les données du formulaire dans les préférences partagées
            writeSharedPref("fistName", editFirstName.text.toString())
            writeSharedPref("lastName", editLastName.text.toString())
            writeSharedPref("email", editEmail.text.toString())
            writeSharedPref("username", editUsername.text.toString())
            writeSharedPref("password", editPassword.text.toString())


            // Mettre à jour la valeur de hasAccount à true pour indiquer que l'utilisateur a un compte
            sharedPreferences.edit().putBoolean("hasAccount", true).apply()
            // Afficher un message de succès

            Toast.makeText(applicationContext, "Données enregistrées avec succès", Toast.LENGTH_SHORT).show()

            // Redirection vers MainActivity
            val intent = Intent(this@CreateAccountActivity, MainActivity::class.java)
            intent.putExtra("fistName", editFirstName.text.toString())
            intent.putExtra("lastName", editFirstName.text.toString())
            intent.putExtra("email", editEmail.text.toString())
            intent.putExtra("username", editUsername.text.toString())
            intent.putExtra("password", editPassword.text.toString())

            startActivity(intent)
        }
    }

    private fun writeSharedPref(key: String, value: String) {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun readSharedPref(key: String): String {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "").toString()
    }
}