package com.example.arosaje

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import java.util.*

class DashboardActivity : BaseActivity() {

    // Déclaration des variables de classe
    private lateinit var textViewTab1: TextView
    private lateinit var textViewTab2: TextView
    private lateinit var textViewTab3: TextView
    private lateinit var textViewTab4: TextView

    // Déclaration des fragments
    private val localisationFragment = LocalisationFragment.newInstance("", "")
    private val rechercheFragment = RechercheFragment.newInstance("", "")
    private val gardeFragment = GardeFragment.newInstance("", "")
    private val myAnnonceFragment = MyAnnonceFragment.newInstance("", "", "", "")

    // Déclaration des variables pour stocker les données
    private var title: String? = null
    private var description: String? = null
    private var startDate: String? = null
    private var endDate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        // Initialisation des variables de classe
        textViewTab1 = findViewById(R.id.textViewTab1)
        textViewTab2 = findViewById(R.id.textViewTab2)
        textViewTab3 = findViewById(R.id.textViewTab3)
        textViewTab4 = findViewById(R.id.textViewTab4)

        // Définition du titre de l'en-tête
        setHeaderTitle(null, R.drawable.logo_arosaje)
        showBack()

        // Attribution des listeners aux TextView
        textViewTab1.setOnClickListener { showMyAnnonces() }
        textViewTab2.setOnClickListener { showRecherche() }
        textViewTab3.setOnClickListener { showLocalisation() }
        textViewTab4.setOnClickListener { showGarde() }

        // Affichage initial
        showRecherche()
    }

    // Méthodes pour afficher les différents fragments
    private fun showLocalisation() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Localisation")
        fragmentTra.replace(R.id.layoutContent, localisationFragment)
        fragmentTra.commit()

        // Mettre à jour la couleur de fond des TextView
        textViewTab1.isSelected = false
        textViewTab2.isSelected = false
        textViewTab3.isSelected = true
        textViewTab4.isSelected = false
    }

    private fun showRecherche() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Recherche")
        fragmentTra.replace(R.id.layoutContent, rechercheFragment)
        fragmentTra.commit()

        // Mettre à jour la couleur de fond des TextView
        textViewTab1.isSelected = false
        textViewTab2.isSelected = true
        textViewTab3.isSelected = false
        textViewTab4.isSelected = false
    }

    private fun showGarde() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()
        fragmentTra.addToBackStack("Garde")
        fragmentTra.replace(R.id.layoutContent, gardeFragment)
        fragmentTra.commit()

        // Mettre à jour la couleur de fond des TextView
        textViewTab1.isSelected = false
        textViewTab2.isSelected = false
        textViewTab3.isSelected = false
        textViewTab4.isSelected = true
    }

    private fun showMyAnnonces() {
        val frManager = supportFragmentManager
        val fragmentTra = frManager.beginTransaction()


        // Passer les données en tant qu'arguments à MyAnnonceFragment
        val bundle = Bundle().apply {
            putString("title", title)
            putString("description", description)
            putString("startDate", startDate)
            putString("endDate", endDate)
        }
        myAnnonceFragment.arguments = bundle

        // Passer les données en tant qu'extra dans l'intention
        val intent = Intent(this, MyAnnonceFragment::class.java).apply {
            putExtra("title", title)
            putExtra("description", description)
            putExtra("startDate", startDate)
            putExtra("endDate", endDate)
        }

        fragmentTra.addToBackStack("MyAnnonces")
        fragmentTra.replace(R.id.layoutContent, myAnnonceFragment)
        fragmentTra.commit()

        // Mettre à jour la couleur de fond des TextView
        textViewTab1.isSelected = true
        textViewTab2.isSelected = false
        textViewTab3.isSelected = false
        textViewTab4.isSelected = false
    }

}
