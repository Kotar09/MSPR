package com.example.arosaje

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment

class MyAnnonceFragment : Fragment() {

    private var title: String? = null
    private var description: String? = null
    private var startDate: String? = null
    private var endDate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_PARAM1)
            description = it.getString(ARG_PARAM2)
            startDate = it.getString(ARG_PARAM3)
            endDate = it.getString(ARG_PARAM4)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_annonces, container, false)

        // Afficher les données dans les TextView du fragment
        view.findViewById<TextView>(R.id.textViewTitle).text = title
        view.findViewById<TextView>(R.id.textViewDescription).text = description
        view.findViewById<TextView>(R.id.textViewStartDate).text = startDate
        view.findViewById<TextView>(R.id.textViewEndDate).text = endDate


        return view
    }


    companion object {
        private const val ARG_PARAM1 = "title"
        private const val ARG_PARAM2 = "description"
        private const val ARG_PARAM3 = "startDate"
        private const val ARG_PARAM4 = "endDate"

        @JvmStatic
        fun newInstance(title: String, description: String, startDate: String, endDate: String) =
            MyAnnonceFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, title)
                    putString(ARG_PARAM2, description)
                    putString(ARG_PARAM3, startDate)
                    putString(ARG_PARAM4, endDate)
                }
            }
    }
}
