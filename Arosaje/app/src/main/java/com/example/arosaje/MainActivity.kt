package com.example.arosaje

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*
import android.content.Context
import android.content.SharedPreferences

class MainActivity : BaseActivity() {

    private lateinit var editTextTitle: EditText
    private lateinit var editTextDescription: EditText
    private lateinit var editTextStartDate: EditText
    private lateinit var editTextEndDate: EditText
    private lateinit var imageViewPlantPhoto: ImageButton
    companion object {
        private const val REQUEST_IMAGE_PICK = 100
        private const val REQUEST_IMAGE_CAPTURE = 101

    }

    private fun writeSharedPref(key: String, value: String) {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun readSharedPref(key: String): String {
        val sharedPreferences: SharedPreferences = getSharedPreferences("account", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "").toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setHeaderTitle(null, R.drawable.logo_arosaje)

        editTextTitle = findViewById(R.id.editTextTitle)
        editTextDescription = findViewById(R.id.editTextDescription)

        editTextStartDate = findViewById(R.id.editTextStartDate)
        editTextEndDate = findViewById(R.id.editTextEndDate)
        editTextStartDate.setOnClickListener {
            showDatePicker(editTextStartDate)
        }

        editTextEndDate.setOnClickListener {
            showDatePicker(editTextEndDate)
        }

        val buttonPhoto = findViewById<Button>(R.id.buttonPhoto)
        buttonPhoto.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }

        imageViewPlantPhoto = findViewById<ImageButton>(R.id.imageViewPlantPhoto)
        imageViewPlantPhoto.setOnClickListener {
            // Créer une intention pour sélectionner une image depuis la galerie
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, REQUEST_IMAGE_PICK)
        }



        val buttonSubmit = findViewById<Button>(R.id.buttonSubmit)
        buttonSubmit.setOnClickListener {
            val title = editTextTitle.text.toString()
            val description = editTextDescription.text.toString()
            val startDate = editTextStartDate.text.toString()
            val endDate = editTextEndDate.text.toString()

            // Enregistrer les données dans les préférences partagées
            writeSharedPref("title", title)
            writeSharedPref("description", description)
            writeSharedPref("startDate", startDate)
            writeSharedPref("endDate", endDate)

            val toastMessage = "Titre: $title\nDescription: $description\nDate de début: $startDate\nDate de fin: $endDate"
            Toast.makeText(this@MainActivity, toastMessage, Toast.LENGTH_SHORT).show()

            // Créer une intention pour démarrer DashboardActivity
            val intent = Intent(this@MainActivity, DashboardActivity::class.java)
            startActivity(intent)
        }


        val userLogoCompte = findViewById<ImageView>(R.id.imageUserLogo)
        userLogoCompte.visibility = View.VISIBLE
        userLogoCompte.setOnClickListener {
            val intent = Intent(this@MainActivity, DashboardActivity::class.java)



            startActivity(intent)
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val selectedImageUri = data?.data
            imageViewPlantPhoto.setImageURI(selectedImageUri)
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap?
            imageViewPlantPhoto.setImageBitmap(imageBitmap)
        }
    }

    private fun showDatePicker(editText: EditText) {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            this,
            { _, year, monthOfYear, dayOfMonth ->
                val selectedDate = Calendar.getInstance()
                selectedDate.set(Calendar.YEAR, year)
                selectedDate.set(Calendar.MONTH, monthOfYear)
                selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val dateString = dateFormat.format(selectedDate.time)

                editText.setText(dateString)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }
}