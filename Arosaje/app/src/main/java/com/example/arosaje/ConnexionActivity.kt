package com.example.arosaje
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Context
import android.content.SharedPreferences
import android.widget.TextView
import org.json.JSONObject
class ConnexionActivity : BaseActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connexion)

        // Initialisation des préférences partagées
        sharedPreferences = getSharedPreferences("user_account", Context.MODE_PRIVATE)

        // Récupération des vues
        val editTextUsername = findViewById<EditText>(R.id.editTextUsername)
        val editTextPassword = findViewById<EditText>(R.id.editTextNom)
        val buttonConnexion = findViewById<Button>(R.id.buttonConnexion)

        // Gestion du clic sur le bouton de connexion
        buttonConnexion.setOnClickListener {
            val username = editTextUsername.text.toString()
            val password = editTextPassword.text.toString()

            // Vérification des informations d'identification
            if (verifyCredentials(username, password)) {
                // Si les informations d'identification sont correctes, connectez l'utilisateur
                loginUser(username)
                // Affichage du toast de connexion réussie
                Toast.makeText(this, "Connexion réussie", Toast.LENGTH_SHORT).show()
            } else {
                // Sinon, affichez un message d'erreur
                Toast.makeText(this, "Identifiants incorrects", Toast.LENGTH_SHORT).show()
            }
        }

        // Gestion du clic sur "Create Account"
        val textViewCreateAccount = findViewById<TextView>(R.id.textViewCreationCompte)
        textViewCreateAccount.setOnClickListener {
            val intent = Intent(this, CreateAccountActivity::class.java)
            startActivity(intent)
        }
    }


    private fun verifyCredentials(username: String, password: String): Boolean {
        // Vérifie si les informations d'identification saisies correspondent aux valeurs attendues
        return username == "test" && password == "1234"
    }

    private fun loginUser(username: String) {
        // enregistrant l'utilisateur connecté dans les préférences partagées ou en lançant l'activité principale

        sharedPreferences.edit().putString("current_user", username).apply()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}