package com.example.arosaje

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class LocalisationFragment : Fragment(){
    lateinit var googleMap: GoogleMap

    private val callback = OnMapReadyCallback { googleMap ->
        this.googleMap = googleMap
        fetchSallesData()
    }

    private val sallesList: MutableList<Salles> = mutableListOf()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun fetchSallesData() {
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://ugarit-online.000webhostapp.com/epsi/films/salles.json")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("SalesFragment", "Error fetching data: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                response.body?.let { responseBody ->
                    val jsonData = responseBody.string()
                    val jsonSalles = JSONObject(jsonData)
                    val sallesArray = jsonSalles.getJSONArray("salles")
                    val markerList = ArrayList<MarkerOptions>()
                    for (i in 0 until sallesArray.length()) {
                        val salleObject = sallesArray.getJSONObject(i)
                        val name = salleObject.getString("name")
                        val address1 = salleObject.getString("address1")
                        val address2 = salleObject.getString("address2")
                        val city = salleObject.getString("city")
                        val parkingInfo = salleObject.getString("parkingInfo")
                        val description = if (salleObject.has("description")) salleObject.getString("description") else ""
                        val publicTransport = salleObject.getString("publicTransport")
                        val latitude = salleObject.getDouble("latitude")
                        val longitude = salleObject.getDouble("longitude")
                        sallesList.add(Salles(name, address1, address2, city, latitude,longitude ,parkingInfo, description, publicTransport))
                        markerList.add(MarkerOptions().position(LatLng(latitude, longitude)).title(name))
                    }
                    activity?.runOnUiThread {
                        addMarkersToMap(markerList)
                    }


                }
            }
        })
    }

    private fun findSalleByName(name: String): Salles? {
        for (salle in sallesList) {
            if (salle.name == name) {
                return salle
            }
        }
        return null
    }

    private fun addMarkersToMap(markerList: List<MarkerOptions>) {
        for (markerOptions in markerList) {
            val marker = googleMap.addMarker(markerOptions)
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(48.854885, 2.338646), 5f))

        googleMap.setOnMarkerClickListener { marker ->
            // Démarrer DetailsActivity lorsque l'utilisateur clique sur un marqueur
            val intent = Intent(activity, DetailsActivity::class.java)
            startActivity(intent)

            // Retourner true pour indiquer que l'événement a été consommé
            true
        }
    }


    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_localisation, container, false)
    }



    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LocalisationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}